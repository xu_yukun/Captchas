package me.Yukun.Captchas;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;

import me.Yukun.Captchas.Handlers.CooldownHandler;
import me.Yukun.Captchas.MultiSupport.Support;

public class GUI implements Listener {
	static ArrayList<String> items = Config.getItemsConfigurationSection("Items");
	static HashMap<Inventory, Integer> chosenslot = new HashMap<Inventory, Integer>();
	static HashMap<Player, Inventory> cplayer = new HashMap<Player, Inventory>();
	static HashMap<Player, Integer> wrong = new HashMap<Player, Integer>();
	static HashMap<Player, Integer> timer = new HashMap<Player, Integer>();
	static HashMap<Inventory, Integer> gracetimer = new HashMap<Inventory, Integer>();
	static HashMap<Inventory, Integer> gracetime = new HashMap<Inventory, Integer>();
	static ArrayList<Player> allowclose = new ArrayList<Player>();
	HashMap<Player, Integer> starttrack = new HashMap<Player, Integer>();
	static Plugin plugin = Bukkit.getPluginManager().getPlugin("Captchas");
	static int chance = Config.getConfigInt("CaptchaOptions.Chance");
	static int time = Config.getConfigInt("CaptchaOptions.Timeout");
	static ItemStack border = makeBorder();
	static ArrayList<Integer> borderslots = getBorderSlots();
	static ArrayList<Integer> clickslots = getClickSlots();
	static HashMap<Inventory, Boolean> invfirst = new HashMap<Inventory, Boolean>();

	public static Boolean isGUI(Inventory inventory) {
		return chosenslot.containsKey(inventory);
	}

	public static void reload() {
		items = Config.getItemsConfigurationSection("Items");
		chance = Config.getConfigInt("CaptchaOptions.Chance");
		time = Config.getConfigInt("CaptchaOptions.Timeout");
		border = makeBorder();
		borderslots = getBorderSlots();
		clickslots = getClickSlots();
		return;
	}

	private static ArrayList<Integer> getBorderSlots() {
		ArrayList<Integer> slots = new ArrayList<Integer>();
		if (Config.getConfigStringList("CaptchaOptions.Border.Slots") != null) {
			for (String line : Config.getConfigStringList("CaptchaOptions.Border.Slots")) {
				if (Api.isInt(line)) {
					slots.add(Integer.parseInt(line));
				}
				continue;
			}
			return slots;
		}
		return slots;
	}

	private static ArrayList<Integer> getClickSlots() {
		ArrayList<Integer> slots = new ArrayList<Integer>();
		for (int i = 1; i <= Config.getConfigInt("CaptchaOptions.GUISize"); ++i) {
			if (borderslots.size() > 0) {
				if (!borderslots.contains(i)) {
					slots.add(i);
				}
				continue;
			} else {
				slots.add(i);
				continue;
			}
		}
		return slots;
	}

	@SuppressWarnings("deprecation")
	private static ItemStack makeBorder() {
		if (Api.getVersion() >= 1131) {
			ItemStack borderitem = new ItemStack(Material.getMaterial(Config.getConfigString("CaptchaOptions.Border.ItemType")), 1);
			ItemMeta borderMeta = borderitem.getItemMeta();
			String name = Api.color(Config.getConfigString("CaptchaOptions.Border.Name"));
			ArrayList<String> lore = new ArrayList<String>();
			for (String line : Config.getConfigStringList("CaptchaOptions.Border.Lore")) {
				lore.add(Api.color(line));
				continue;
			}
			borderMeta.setDisplayName(name);
			borderMeta.setLore(lore);
			borderitem.setItemMeta(borderMeta);
			return borderitem;
		} else {
			String smat = Config.getConfigString("CaptchaOptions.Border.ItemType");
			Material mat = Material.AIR;
			byte data = 16;
			if (smat.contains(":")) {
				String[] smata = smat.split(":");
				mat = Material.getMaterial(smata[0]);
				data = Byte.parseByte(smata[1]);
			} else {
				mat = Material.getMaterial(smat);
			}
			ItemStack borderitem = new ItemStack(mat, 1);
			if (data != 16) {
				borderitem = new ItemStack(mat, 1, data);
			}
			ItemMeta borderMeta = borderitem.getItemMeta();
			String name = Api.color(Config.getConfigString("CaptchaOptions.Border.Name"));
			ArrayList<String> lore = new ArrayList<String>();
			for (String line : Config.getConfigStringList("CaptchaOptions.Border.Lore")) {
				lore.add(Api.color(line));
				continue;
			}
			borderMeta.setDisplayName(name);
			borderMeta.setLore(lore);
			borderitem.setItemMeta(borderMeta);
			return borderitem;
		}
	}

	public static Boolean isCaptcha(Player player) {
		return timer.containsKey(player);
	}

	public static Integer getWrong(Player player) {
		if (wrong.containsKey(player)) {
			return wrong.get(player);
		} else {
			return null;
		}
	}

	private static Boolean isGrace(Inventory inventory) {
		return gracetime.containsKey(inventory);
	}

	private static void removeGrace(Inventory inventory) {
		gracetime.remove(inventory);
		Bukkit.getScheduler().cancelTask(gracetimer.get(inventory));
		gracetimer.remove(inventory);
		return;
	}

	public static void closeCaptcha(Player player) {
		if (player.getOpenInventory() != null) {
			Inventory captcha = player.getOpenInventory().getTopInventory();
			allowclose.remove(player);
			player.closeInventory();
			if (invfirst.containsKey(captcha)) {
				if (invfirst.get(captcha)) {
					Config.addPlayer(player);
				}
				invfirst.remove(captcha);
			}
			if (gracetime.containsKey(captcha)) {
				int gtimer = gracetimer.get(captcha);
				Bukkit.getScheduler().cancelTask(gtimer);
				gracetime.remove(captcha);
				gracetimer.remove(captcha);
			}
			if (timer.containsKey(player)) {
				int ptimer = timer.get(player);
				Bukkit.getScheduler().cancelTask(ptimer);
				timer.remove(player);
			}

			return;
		}
	}

	private static ItemStack makeChosen(ItemStack item) {
		if (Config.getConfigConfigurationSection("CaptchaOptions.Picked") != null) {
			ItemMeta itemMeta = item.getItemMeta();
			if (Config.getConfigString("CaptchaOptions.Picked.Name") != null) {
				String dname = Api.color(Config.getConfigString("CaptchaOptions.Picked.Name"));
				itemMeta.setDisplayName(dname);
			}
			if (Config.getConfigStringList("CaptchaOptions.Picked.Lore") != null) {
				ArrayList<String> dlore = new ArrayList<String>();
				for (String line : Config.getConfigStringList("CaptchaOptions.Picked.Lore")) {
					dlore.add(Api.color(line));
				}
				itemMeta.setLore(dlore);
			}
			item.setItemMeta(itemMeta);
			if (Config.getConfigBoolean("CaptchaOptions.Picked.Glowing")) {
				return Support.addGlow(item);
			}
		}
		return item;
	}

	public static void openCaptcha(Player player, Boolean first) {
		player.sendMessage(Api.color(Config.getMessageString("Messages.Prefix") + Config.getMessageString("Messages.Open")));
		int guiSize = Config.getConfigInt("CaptchaOptions.GUISize");
		int choose = Api.getRandomNumber(clickslots.size());
		int chosen = clickslots.get(choose);
		Collections.shuffle(items);
		List<String> items2 = items.subList(0, clickslots.size());
		String guiName = Api.color(Api.replaceItemName(Config.getConfigString("CaptchaOptions.GUIName"), items2.get(choose)));
		if (guiName != null) {
			Inventory captcha = Bukkit.createInventory(null, guiSize, guiName);
			chosenslot.put(captcha, chosen - 1);
			if (Config.getConfigInt("CaptchaOptions.Grace") > 0) {
				int grace = Config.getConfigInt("CaptchaOptions.Grace");
				gracetime.put(captcha, grace);
				gracetimer.put(captcha, Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable() {
					public void run() {
						if (gracetime.get(captcha) > 0) {
							gracetime.put(captcha, gracetime.get(captcha) - 1);
							return;
						} else {
							removeGrace(captcha);
							return;
						}
					}
				}, 20, 20));
			}
			allowclose.add(player);
			for (int i = 0; i < clickslots.size(); ++i) {
				try {
					captcha.setItem(clickslots.get(i) - 1, new ItemStack(Material.getMaterial(items2.get(i))));
				} catch (IllegalArgumentException e) {
					player.sendMessage("error: " + items2.get(i));
				} catch (NullPointerException e) {
					player.sendMessage("error: " + items2.get(i));
				}
				continue;
			}
			if (borderslots.size() > 0) {
				for (int i : borderslots) {
					int slot = i - 1;
					captcha.setItem(slot, border);
					continue;
				}
			}
			ItemStack citem = captcha.getItem(chosen - 1);
			ItemStack dcitem = makeChosen(citem);
			captcha.setItem(chosen - 1, dcitem);
			if (captcha.getItem(guiSize - 1).getType() != Material.AIR) {
				player.teleport(player.getLocation());
				player.openInventory(captcha);
				invfirst.put(captcha, first);
				timer.put(player, Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					public void run() {
						allowclose.remove(player);
						player.closeInventory();
						if (invfirst.get(captcha)) {
							invfirst.remove(captcha);
							for (String line : Main.settings.getConfig().getStringList("CaptchaOptions.Commands")) {
								Bukkit.dispatchCommand(Bukkit.getConsoleSender(), line.replace("%player%", player.getName()));
							}
							player.sendMessage(Api.color(Config.getMessageString("Messages.Prefix") + Config.getMessageString("Messages.Punish")));
							return;
						}
						if (Config.getConfigInt("CaptchaOptions.Wrong") <= 1) {
							for (String line : Main.settings.getConfig().getStringList("CaptchaOptions.Commands")) {
								Bukkit.dispatchCommand(Bukkit.getConsoleSender(), line.replace("%player%", player.getName()));
							}
							player.sendMessage(Api.color(Config.getMessageString("Messages.Prefix") + Config.getMessageString("Messages.Wrong") + Api.replaceStrikes(player, Config.getMessageString("Messages.Strike"))));
							player.sendMessage(Api.color(Config.getMessageString("Messages.Prefix") + Config.getMessageString("Messages.Punish")));
							timer.remove(player);
							invfirst.remove(captcha);
							return;
						} else {
							if (wrong.containsKey(player)) {
								wrong.put(player, wrong.get(player) + 1);
								player.sendMessage(Api.color(Config.getMessageString("Messages.Prefix") + Config.getMessageString("Messages.Wrong") + Api.replaceStrikes(player, Config.getMessageString("Messages.Strike"))));
								if (wrong.get(player) >= Config.getConfigInt("CaptchaOptions.Wrong")) {
									for (String line : Main.settings.getConfig().getStringList("CaptchaOptions.Commands")) {
										Bukkit.dispatchCommand(Bukkit.getConsoleSender(), line.replace("%player%", player.getName()));
									}
									player.sendMessage(Api.color(Config.getMessageString("Messages.Prefix") + Config.getMessageString("Messages.Punish")));
									wrong.remove(player);
									timer.remove(player);
									invfirst.remove(captcha);
									return;
								}
								timer.remove(player);
								return;
							} else {
								wrong.put(player, 1);
								player.sendMessage(Api.color(Config.getMessageString("Messages.Prefix") + Config.getMessageString("Messages.Wrong") + Api.replaceStrikes(player, Config.getMessageString("Messages.Strike"))));
								if (wrong.get(player) >= Config.getConfigInt("CaptchaOptions.Wrong")) {
									for (String line : Main.settings.getConfig().getStringList("CaptchaOptions.Commands")) {
										Bukkit.dispatchCommand(Bukkit.getConsoleSender(), line.replace("%player%", player.getName()));
									}
									player.sendMessage(Api.color(Config.getMessageString("Messages.Prefix") + Config.getMessageString("Messages.Punish")));
									wrong.remove(player);
									timer.remove(player);
									invfirst.remove(captcha);
									return;
								}
								timer.remove(player);
								return;
							}
						}
					}

				}, time * 20));
				return;
			} else {
				return;
			}
		}

	}

	@EventHandler
	public void playerQuitEvent(PlayerQuitEvent e) {
		if (allowclose.contains(e.getPlayer())) {
			Player player = e.getPlayer();
			for (String line : Main.settings.getConfig().getStringList("CaptchaOptions.Commands")) {
				Bukkit.dispatchCommand(Bukkit.getConsoleSender(), line.replace("%player%", player.getName()));
			}
			if (wrong.containsKey(player)) {
				wrong.remove(player);
			}
		}
	}

	@EventHandler
	public void CaptchaCloseEvent(InventoryCloseEvent e) {
		if (allowclose.contains(e.getPlayer())) {
			if (chosenslot.containsKey(e.getInventory())) {
				Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					public void run() {
						e.getPlayer().openInventory(e.getInventory());
						return;
					}
				}, 1);
			}
		}
	}

	@EventHandler
	public void CaptchaClickEvent(InventoryClickEvent e) {
		if (e.getCurrentItem() != null) {
			if (e.getCurrentItem().isSimilar(border)) {
				e.setCancelled(true);
				return;
			}
		}
		if (chosenslot.containsKey(e.getInventory())) {
			if (chosenslot.containsKey(e.getClickedInventory())) {
				if (!(e.getClickedInventory() instanceof PlayerInventory)) {
					e.setCancelled(true);
					if (e.getSlot() == chosenslot.get(e.getInventory())) {
						Player player = (Player) e.getWhoClicked();
						allowclose.remove(player);
						if (invfirst.get(e.getInventory())) {
							Config.addPlayer(player);
						}
						if (timer.get(player) != null) {
							Bukkit.getScheduler().cancelTask(timer.get(player));
							timer.remove(player);
						}
						player.closeInventory();
						if (Main.settings.getConfig().getStringList("CaptchaOptions.Bonus") != null) {
							for (String line : Main.settings.getConfig().getStringList("CaptchaOptions.Bonus")) {
								Bukkit.dispatchCommand(Bukkit.getConsoleSender(), line.replace("%player%", player.getName()));
							}
						}
						if (!Config.getConfigBoolean("CaptchaOptions.Clear")) {
							if (wrong.containsKey(player)) {
								CooldownHandler.startCooldown(player);
								player.sendMessage(Api.color(Config.getMessageString("Messages.Prefix") + Config.getMessageString("Messages.Right") + Api.replaceStrikes(player, Config.getMessageString("Messages.Clear"))));
								invfirst.remove(e.getInventory());
								return;
							} else {
								CooldownHandler.startCooldown(player);
								player.sendMessage(Api.color(Config.getMessageString("Messages.Prefix") + Config.getMessageString("Messages.Right")));
								invfirst.remove(e.getInventory());
								return;
							}
						} else {
							if (wrong.containsKey(player)) {
								if (wrong.get(player) > 1) {
									wrong.put(player, wrong.get(player) - 1);
									CooldownHandler.startCooldown(player);
									player.sendMessage(Api.color(Config.getMessageString("Messages.Prefix") + Config.getMessageString("Messages.Right") + Api.replaceStrikes(player, Config.getMessageString("Messages.Clear"))));
									invfirst.remove(e.getInventory());
									return;
								} else {
									wrong.remove(player);
									CooldownHandler.startCooldown(player);
									player.sendMessage(Api.color(Config.getMessageString("Messages.Prefix") + Config.getMessageString("Messages.Right") + Api.replaceStrikes(player, Config.getMessageString("Messages.Clear"))));
									invfirst.remove(e.getInventory());
									return;
								}
							} else {
								CooldownHandler.startCooldown(player);
								player.sendMessage(Api.color(Config.getMessageString("Messages.Prefix") + Config.getMessageString("Messages.Right")));
								invfirst.remove(e.getInventory());
								return;
							}
						}
					} else {
						Player player = (Player) e.getWhoClicked();
						if (timer.get(player) != null) {
							Bukkit.getScheduler().cancelTask(timer.get(player));
							timer.remove(player);
						}
						if (!isGrace(e.getInventory())) {
							allowclose.remove(player);
							player.closeInventory();
							if (invfirst.get(e.getInventory())) {
								invfirst.remove(e.getInventory());
								for (String line : Main.settings.getConfig().getStringList("CaptchaOptions.Commands")) {
									Bukkit.dispatchCommand(Bukkit.getConsoleSender(), line.replace("%player%", player.getName()));
								}
								player.sendMessage(Api.color(Config.getMessageString("Messages.Prefix") + Config.getMessageString("Messages.Punish")));
								return;
							}
							if (Config.getConfigInt("CaptchaOptions.Wrong") <= 1) {
								for (String line : Main.settings.getConfig().getStringList("CaptchaOptions.Commands")) {
									Bukkit.dispatchCommand(Bukkit.getConsoleSender(), line.replace("%player%", player.getName()));
								}
								player.sendMessage(Api.color(Config.getMessageString("Messages.Prefix") + Config.getMessageString("Messages.Wrong") + Api.replaceStrikes(player, Config.getMessageString("Messages.Strike"))));
								player.sendMessage(Api.color(Config.getMessageString("Messages.Prefix") + Config.getMessageString("Messages.Punish")));
								invfirst.remove(e.getInventory());
								return;
							} else {
								if (wrong.containsKey(player)) {
									wrong.put(player, wrong.get(player) + 1);
									player.sendMessage(Api.color(Config.getMessageString("Messages.Prefix") + Config.getMessageString("Messages.Wrong") + Api.replaceStrikes(player, Config.getMessageString("Messages.Strike"))));
									if (wrong.get(player) >= Config.getConfigInt("CaptchaOptions.Wrong")) {
										for (String line : Main.settings.getConfig().getStringList("CaptchaOptions.Commands")) {
											Bukkit.dispatchCommand(Bukkit.getConsoleSender(), line.replace("%player%", player.getName()));
										}
										player.sendMessage(Api.color(Config.getMessageString("Messages.Prefix") + Config.getMessageString("Messages.Punish")));
										wrong.remove(player);
										invfirst.remove(e.getInventory());
										return;
									}
									return;
								} else {
									wrong.put(player, 1);
									player.sendMessage(Api.color(Config.getMessageString("Messages.Prefix") + Config.getMessageString("Messages.Wrong") + Api.replaceStrikes(player, Config.getMessageString("Messages.Strike"))));
									if (wrong.get(player) >= Config.getConfigInt("CaptchaOptions.Wrong")) {
										for (String line : Main.settings.getConfig().getStringList("CaptchaOptions.Commands")) {
											Bukkit.dispatchCommand(Bukkit.getConsoleSender(), line.replace("%player%", player.getName()));
										}
										player.sendMessage(Api.color(Config.getMessageString("Messages.Prefix") + Config.getMessageString("Messages.Punish")));
										wrong.remove(player);
										invfirst.remove(e.getInventory());
										return;
									}
									return;
								}
							}
						} else {
							player.sendMessage(Api.color(Config.getMessageString("Messages.Prefix") + Config.getMessageString("Messages.Grace")));
							return;
						}
					}
				} else {
					e.setCancelled(true);
					return;
				}
			} else {
				e.setCancelled(true);
				return;
			}
		}
	}
}