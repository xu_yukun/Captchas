package me.Yukun.Captchas.MultiSupport;

import org.bukkit.craftbukkit.v1_9_R1.inventory.CraftItemStack;
import org.bukkit.inventory.ItemStack;

import net.minecraft.server.v1_9_R1.NBTTagList;
import net.minecraft.server.v1_9_R1.NBTTagCompound;

public class NMS_1_9_R1 {
	/**
	 * Enchantment glow adding method from https://www.spigotmc.org/threads/solved-enchant-item-without-having-enchantment-shown.36193/
	 * 
	 * @param item
	 *            Item to add enchantment glow.
	 * @return ItemStack with glow added to it.
	 */
	public static ItemStack addGlow(ItemStack item) {
		net.minecraft.server.v1_9_R1.ItemStack nmsStack = CraftItemStack.asNMSCopy(item);
		NBTTagCompound tag = null;
		if (!nmsStack.hasTag()) {
			tag = new NBTTagCompound();
			nmsStack.setTag(tag);
		}
		if (tag == null)
			tag = nmsStack.getTag();
		NBTTagList ench = new NBTTagList();
		tag.set("ench", ench);
		nmsStack.setTag(tag);
		return CraftItemStack.asCraftMirror(nmsStack);
	}
}