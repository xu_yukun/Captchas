package me.Yukun.Captchas.MultiSupport;

import org.bukkit.Bukkit;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import me.Yukun.Captchas.Api;

public class Support {
	public static Boolean hasAuthMe() {
		return Bukkit.getPluginManager().getPlugin("AuthMe") != null;
	}

	public static Boolean hasLockLogin() {
		return Bukkit.getPluginManager().isPluginEnabled("LockLogin");
	}

	public static ItemStack addGlow(ItemStack item) {
		int version = Api.getVersion();
		if (version >= 1111) {
			ItemMeta itemMeta = item.getItemMeta();
			itemMeta.addEnchant(Enchantment.DURABILITY, 1, false);
			itemMeta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
			item.setItemMeta(itemMeta);
			return item;
		}
		if (version == 174) {
			return NMS_1_7_R4.addGlow(item);
		}
		if (version == 181) {
			return NMS_1_8_R1.addGlow(item);
		}
		if (version == 182) {
			return NMS_1_8_R2.addGlow(item);
		}
		if (version == 183) {
			return NMS_1_8_R3.addGlow(item);
		}
		if (version == 191) {
			return NMS_1_9_R1.addGlow(item);
		}
		if (version == 192) {
			return NMS_1_9_R2.addGlow(item);
		}
		if (version == 1101) {
			return NMS_1_10_R1.addGlow(item);
		}
		return item;
	}
}