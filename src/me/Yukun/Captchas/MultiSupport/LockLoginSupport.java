package me.Yukun.Captchas.MultiSupport;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import io.github.karmaconfigs.Spigot.API.PlayerAPI;
import me.Yukun.Captchas.Handlers.DelayHandler;

public class LockLoginSupport implements Listener {
	private ArrayList<Player> tracking = new ArrayList<Player>();
	private ArrayList<Integer> trackers = new ArrayList<Integer>();

	private PlayerAPI lockplayer(Player player) {
		return new PlayerAPI(player);
	}

	@EventHandler
	private void playerJoinEvent(PlayerJoinEvent e) {
		tracking.add(e.getPlayer());
		return;
	}

	@EventHandler
	private void playerQuitEvent(PlayerQuitEvent e) {
		if (tracking.contains(e.getPlayer())) {
			tracking.remove(e.getPlayer());
			return;
		}
	}

	@EventHandler
	private void playerCommandEvent(PlayerCommandPreprocessEvent e) {
		if (e.getMessage().startsWith("/register")) {
			if (tracking.contains(e.getPlayer())) {
				PlayerAPI player = lockplayer(e.getPlayer());
				trackers.add(Bukkit.getScheduler().scheduleSyncDelayedTask(Bukkit.getPluginManager().getPlugin("Captchas"), new Runnable() {
					public void run() {
						if (player.isLogged()) {
							DelayHandler.openCaptcha(e.getPlayer(), true);
							tracking.remove(e.getPlayer());
							return;
						}
					}
				}, 1));
			}
		}
	}
}