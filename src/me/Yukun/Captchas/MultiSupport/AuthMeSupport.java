package me.Yukun.Captchas.MultiSupport;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import fr.xephi.authme.events.LoginEvent;
import me.Yukun.Captchas.Config;
import me.Yukun.Captchas.Handlers.DelayHandler;

public class AuthMeSupport implements Listener {
	@EventHandler
	public void onLogin(LoginEvent e) {
		Player player = e.getPlayer();
		if (Config.getPlayers() != null) {
			if (!Config.getPlayers().contains(player)) {
				DelayHandler.openCaptcha(player, true);
				return;
			}
		} else {
			DelayHandler.openCaptcha(player, true);
			return;
		}
	}
}