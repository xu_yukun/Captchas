package me.Yukun.Captchas.Handlers;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.Yukun.Captchas.Api;
import me.Yukun.Captchas.Config;
import me.Yukun.Captchas.GUI;
import me.Yukun.Captchas.Main;
import me.Yukun.Captchas.Triggers.BlockBreakEventTrigger;
import me.Yukun.Captchas.Triggers.EntityDamageByEntityEventTrigger;
import me.Yukun.Captchas.Triggers.PlayerFishEventTrigger;

public class CommandHandler implements CommandExecutor {
	private String line1 = "&b&l==========Captchas by Yukun==========";
	private String line2 = "&bAliases: &ccaptchas, captcha, cap";
	private String line3 = "&c/captchas (player): Opens the captcha GUI for specified player.";
	private String line4 = "&c/captchas reload: Reloads the config files.";
	private String line5 = "&c/captchas close (player): Closes the captcha GUI for specified player.";
	private String line6 = "&b&l=====================================";
	private String prefix = Api.color(Config.getMessageString("Messages.Prefix"));
	private String invalid = Api.color("&cInvalid argument!");
	private String reload = Api.color("&aConfig files reloaded!");
	private String cooldown = Api.color("&cPlease wait for this player to complete the previous captcha!");

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		if (args.length == 0) {
			if (sender.isOp() || sender.hasPermission("Captchas.Admin")) {
				sender.sendMessage(Api.color(line1));
				sender.sendMessage(Api.color(line2));
				sender.sendMessage(Api.color(line3));
				if (sender.isOp()) {
					sender.sendMessage(Api.color(line4));
					sender.sendMessage(Api.color(line5));
				}
				sender.sendMessage(Api.color(line6));
				return true;
			}
			sender.sendMessage(prefix + invalid);
			return false;
		} else if (args.length == 1) {
			if (args[0].equalsIgnoreCase("reload")) {
				if (sender.isOp()) {
					Main.settings.setup(Bukkit.getPluginManager().getPlugin("Captchas"));
					Main.settings.reloadConfig();
					Main.settings.reloadItems();
					Main.settings.reloadMessages();
					Main.settings.reloadPlayers();
					BlockBreakEventTrigger.setup();
					GUI.reload();
					EntityDamageByEntityEventTrigger.reload();
					PlayerFishEventTrigger.reload();
					prefix = Api.color(Config.getMessageString("Messages.Prefix"));
					sender.sendMessage(prefix + reload);
					return true;
				} else {
					sender.sendMessage(prefix + invalid);
					return false;
				}
			} else if (Bukkit.getPlayer(args[0]) != null) {
				if (sender.isOp() || sender.hasPermission("captchas.admin")) {
					Player player = Bukkit.getPlayer(args[0]);
					if (!DelayHandler.isDelay(player) && !GUI.isCaptcha(player)) {
						DelayHandler.openCaptcha(player, false);
						return true;
					} else {
						sender.sendMessage(prefix + cooldown);
						return false;
					}
				} else {
					sender.sendMessage(prefix + invalid);
					return false;
				}
			} else {
				sender.sendMessage(prefix + invalid);
				return false;
			}
		} else if (args.length == 2) {
			if (args[0].equalsIgnoreCase("close")) {
				if (sender.isOp() || sender.hasPermission("captchas.admin")) {
					if (Bukkit.getPlayer(args[1]) != null) {
						CloseHandler.closeCaptcha(Bukkit.getPlayer(args[1]));
						sender.sendMessage(Api.color(prefix + Config.getMessageString("Messages.Closed")));
						Bukkit.getPlayer(args[1]).sendMessage(Api.color(prefix + Config.getMessageString("Messages.Close")));
						return true;
					} else {
						sender.sendMessage(prefix + invalid);
						return false;
					}
				} else {
					sender.sendMessage(prefix + invalid);
					return false;
				}
			} else {
				sender.sendMessage(prefix + invalid);
				return false;
			}
		} else {
			sender.sendMessage(prefix + invalid);
			return false;
		}
	}
}