package me.Yukun.Captchas.Handlers;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;

import me.Yukun.Captchas.Config;
import me.Yukun.Captchas.GUI;

public class FreezeHandler implements Listener {
	@EventHandler
	private void freezeMoveEvent(PlayerMoveEvent e) {
		if (e.getPlayer() != null) {
			Player player = e.getPlayer();
			if (Config.getPlayers() != null) {
				if (!Config.getPlayers().contains(player)) {
					if (!player.isOp() && !player.hasPermission("Captchas.Admin") && !player.hasPermission("Captchas.Bypass")) {
						e.setCancelled(true);
						return;
					}
				}
			}
		}
	}

	@EventHandler
	private void freezeInteractEvent(PlayerInteractEvent e) {
		if (e.getPlayer() != null) {
			Player player = e.getPlayer();
			if (Config.getPlayers() != null) {
				if (!Config.getPlayers().contains(player)) {
					if (!player.isOp() && !player.hasPermission("Captchas.Admin") && !player.hasPermission("Captchas.Bypass")) {
						e.setCancelled(true);
						return;
					}
				}
			}
		}
	}

	@EventHandler
	private void freezeDamageEvent(EntityDamageByEntityEvent e) {
		if (e.getDamager() instanceof Player) {
			Player player = (Player) e.getDamager();
			if (Config.getPlayers() != null) {
				if (!Config.getPlayers().contains(player)) {
					if (!player.isOp() && !player.hasPermission("Captchas.Admin") && !player.hasPermission("Captchas.Bypass")) {
						e.setCancelled(true);
						return;
					}
				}
			}
		}
	}

	@EventHandler
	private void freezeChatEvent(AsyncPlayerChatEvent e) {
		if (e.getPlayer() != null) {
			Player player = e.getPlayer();
			if (Config.getPlayers() != null) {
				if (!Config.getPlayers().contains(player)) {
					if (!player.isOp() && !player.hasPermission("Captchas.Admin") && !player.hasPermission("Captchas.Bypass")) {
						e.setCancelled(true);
						return;
					}
				}
			}
		}
	}

	@EventHandler
	private void freezeInventoryOpenEvent(InventoryOpenEvent e) {
		if (e.getPlayer() != null) {
			Player player = (Player) e.getPlayer();
			if (Config.getPlayers() != null) {
				if (!Config.getPlayers().contains(player)) {
					if (e.getInventory() != null) {
						if (!GUI.isGUI(e.getInventory())) {
							if (!player.isOp() && !player.hasPermission("Captchas.Admin") && !player.hasPermission("Captchas.Bypass")) {
								e.setCancelled(true);
								return;
							}
						}
					}
				}
			}
		}
	}
}