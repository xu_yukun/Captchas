package me.Yukun.Captchas.Handlers;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import me.Yukun.Captchas.GUI;

public class CloseHandler {
	public static void closeCaptcha(Player player) {
		if (GUI.isCaptcha(player)) {
			GUI.closeCaptcha(player);
		}
		if (DelayHandler.isDelay(player)) {
			DelayHandler.closeCaptcha(player);
		}
	}

	public static void closeAllCaptchas() {
		for (Player player : Bukkit.getOnlinePlayers()) {
			closeCaptcha(player);
		}
		return;
	}
}