package me.Yukun.Captchas.Handlers;

import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import me.Yukun.Captchas.Config;

public class CooldownHandler {
	private static ArrayList<Player> cooldown = new ArrayList<>();
	private static HashMap<Player, Integer> timer = new HashMap<Player, Integer>();
	private static Plugin plugin = Bukkit.getPluginManager().getPlugin("Captchas");

	public static void startCooldown(Player player) {
		if (Config.getConfigInt("CaptchaOptions.Cooldown") > 0) {
			if (!cooldown.contains(player)) {
				cooldown.add(player);
				timer.put(player, Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					public void run() {
						cooldown.remove(player);
						return;
					}
				}, 20 * Config.getConfigInt("CaptchaOptions.Cooldown")));
				return;
			} else {
				cooldown.remove(player);
				Bukkit.getScheduler().cancelTask(timer.get(player));
				cooldown.add(player);
				timer.put(player, Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					public void run() {
						cooldown.remove(player);
						return;
					}
				}, 20 * Config.getConfigInt("CaptchaOptions.Cooldown")));
				return;
			}
		} else {
			return;
		}
	}

	public static Boolean isCooldown(Player player) {
		return cooldown.contains(player);
	}
}