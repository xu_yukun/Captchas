package me.Yukun.Captchas.Handlers;

import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import me.Yukun.Captchas.Api;
import me.Yukun.Captchas.Config;
import me.Yukun.Captchas.GUI;
import me.Yukun.Captchas.Main;

public class DelayHandler implements Listener {
	private static HashMap<Player, Integer> timers = new HashMap<Player, Integer>();

	public static Boolean isDelay(Player player) {
		return timers.containsKey(player);
	}

	public static void closeCaptcha(Player player) {
		if (isDelay(player)) {
			int timer = timers.get(player);
			Bukkit.getScheduler().cancelTask(timer);
			timers.remove(player);
		}
		return;
	}

	public static void openCaptcha(Player player, Boolean first) {
		if (!player.hasPermission("Captchas.Bypass") && !player.hasPermission("Captchas.Admin") && !player.isOp()) {
			if (Config.getConfigBoolean("CaptchaOptions.Warning.Enable")) {
				if (!timers.containsKey(player)) {
					if (!GUI.isCaptcha(player)) {
						int delay = Config.getConfigInt("CaptchaOptions.Warning.Delay");
						player.sendMessage(Api.color(Config.getMessageString("Messages.Prefix") + Config.getMessageString("Messages.Warning")));
						timers.put(player, Bukkit.getScheduler().scheduleSyncDelayedTask(Bukkit.getPluginManager().getPlugin("Captchas"), new Runnable() {
							public void run() {
								GUI.openCaptcha(player, first);
								timers.remove(player);
								return;
							}
						}, delay * 20));
					} else {
						return;
					}
				} else {
					return;
				}
			} else {
				GUI.openCaptcha(player, first);
				return;
			}
		}
		return;
	}

	@EventHandler
	private void playerQuitEvent(PlayerQuitEvent e) {
		Player player = e.getPlayer();
		if (timers.containsKey(player)) {
			Bukkit.getScheduler().cancelTask(timers.get(player));
			timers.remove(player);
			for (String line : Main.settings.getConfig().getStringList("CaptchaOptions.Commands")) {
				Bukkit.dispatchCommand(Bukkit.getConsoleSender(), line.replace("%player%", player.getName()));
			}
			return;
		}
	}
}