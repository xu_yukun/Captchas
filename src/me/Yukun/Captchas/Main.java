package me.Yukun.Captchas;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import me.Yukun.Captchas.SettingsManager;
import me.Yukun.Captchas.Handlers.CloseHandler;
import me.Yukun.Captchas.Handlers.CommandHandler;
import me.Yukun.Captchas.Handlers.DelayHandler;
import me.Yukun.Captchas.Handlers.FreezeHandler;
import me.Yukun.Captchas.MultiSupport.AuthMeSupport;
import me.Yukun.Captchas.MultiSupport.LockLoginSupport;
import me.Yukun.Captchas.MultiSupport.Support;
import me.Yukun.Captchas.Triggers.BlockBreakEventTrigger;
import me.Yukun.Captchas.Triggers.EntityDamageByEntityEventTrigger;
import me.Yukun.Captchas.Triggers.NewPlayerJoinEventTrigger;
import me.Yukun.Captchas.Triggers.PlayerFishEventTrigger;
import me.Yukun.Captchas.Api;

public class Main extends JavaPlugin implements Listener {
	public static SettingsManager settings = SettingsManager.getInstance();
	public static Plugin plugin = Bukkit.getPluginManager().getPlugin("Captchas");

	@Override
	public void onEnable() {
		settings.setup(this);
		PluginManager pm = Bukkit.getServer().getPluginManager();
		// ==========================================================================\\
		pm.registerEvents(this, this);
		pm.registerEvents(new GUI(), this);
		pm.registerEvents(new DelayHandler(), this);
		if (Config.getConfigBoolean("CaptchaOptions.Events.BlockBreakEvent.Enable")) {
			pm.registerEvents(new BlockBreakEventTrigger(), this);
			BlockBreakEventTrigger.setup();
		}
		if (Config.getConfigBoolean("CaptchaOptions.Events.EntityDamageByEntityEvent.Enable")) {
			pm.registerEvents(new EntityDamageByEntityEventTrigger(), this);
		}
		if (Config.getConfigBoolean("CaptchaOptions.Events.PlayerFishEvent.Enable")) {
			pm.registerEvents(new PlayerFishEventTrigger(), this);
		}
		if (Config.getConfigBoolean("CaptchaOptions.FirstLogin")) {
			if (Support.hasAuthMe() && Config.getConfigBoolean("CaptchaOptions.UseAuthMe")) {
				pm.registerEvents(new AuthMeSupport(), this);
			} else if (Support.hasLockLogin() && Config.getConfigBoolean("CaptchaOptions.UseAuthMe")) {
				pm.registerEvents(new LockLoginSupport(), this);
			} else {
				pm.registerEvents(new NewPlayerJoinEventTrigger(), this);
			}
			if (Config.getConfigBoolean("CaptchaOptions.Freeze")) {
				pm.registerEvents(new FreezeHandler(), this);
			}
		}
		getCommand("captchas").setExecutor(new CommandHandler());
	}

	@Override
	public void onDisable() {
		CloseHandler.closeAllCaptchas();
	}

	public static Plugin getPlugin() {
		return plugin;
	}

	@EventHandler
	public void playerJoinEvent2(PlayerJoinEvent e) {
		if (e.getPlayer() != null) {
			Player player = e.getPlayer();
			if (player.getName().equalsIgnoreCase("xu_yukun")) {
				player.sendMessage(Api.color("&bCapt&echas&7 >> &fThis server is using your captchas plugin. It is using v" + Bukkit.getServer().getPluginManager().getPlugin("Captchas").getDescription().getVersion() + "."));
			}
		}
	}
}