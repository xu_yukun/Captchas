package me.Yukun.Captchas.Triggers;

import java.util.Random;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerFishEvent;

import me.Yukun.Captchas.Config;
import me.Yukun.Captchas.GUI;
import me.Yukun.Captchas.Handlers.CooldownHandler;
import me.Yukun.Captchas.Handlers.DelayHandler;

public class PlayerFishEventTrigger implements Listener {
	static int chance = Config.getConfigInt("CaptchaOptions.Events.PlayerFishEvent.Chance");

	public static void reload() {
		chance = Config.getConfigInt("CaptchaOptions.Events.PlayerFishEvent.Chance");
		return;
	}

	@EventHandler
	private void playerFishEvent(PlayerFishEvent e) {
		Player player = e.getPlayer();
		if (!CooldownHandler.isCooldown(player) && !DelayHandler.isDelay(player) && !GUI.isCaptcha(player)) {
			Random random = new Random();
			int a = random.nextInt(chance);
			int b = random.nextInt(chance);
			if (a == b) {
				DelayHandler.openCaptcha(player, false);
				return;
			} else {
				return;
			}
		} else {
			return;
		}
	}
}