package me.Yukun.Captchas.Triggers;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import me.Yukun.Captchas.Config;
import me.Yukun.Captchas.Handlers.DelayHandler;

public class NewPlayerJoinEventTrigger implements Listener {
	@EventHandler
	private void newPlayerJoinEvent(PlayerJoinEvent e) {
		Player player = e.getPlayer();
		if (Config.getPlayers() != null) {
			if (!Config.getPlayers().contains(player)) {
				DelayHandler.openCaptcha(player, true);
				return;
			}
		} else {
			DelayHandler.openCaptcha(player, true);
			return;
		}
	}
}