package me.Yukun.Captchas.Triggers;

import java.util.ArrayList;
import java.util.Random;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

import me.Yukun.Captchas.Config;
import me.Yukun.Captchas.GUI;
import me.Yukun.Captchas.Handlers.CooldownHandler;
import me.Yukun.Captchas.Handlers.DelayHandler;

public class BlockBreakEventTrigger implements Listener {
	static int chance = Config.getConfigInt("CaptchaOptions.Events.BlockBreakEvent.Chance");
	static ArrayList<String> legacyBlocks = new ArrayList<String>();
	static ArrayList<String> blocks = new ArrayList<String>();
	static Boolean isLegacy = false;
	static Boolean invert = false;

	public static void setup() {
		chance = Config.getConfigInt("CaptchaOptions.Events.BlockBreakEvent.Chance");
		invert = Config.getConfigBoolean("CaptchaOptions.Invert");
		if (Config.getConfigBoolean("CaptchaOptions.Events.BlockBreakEvent.Filter")) {
			if (Config.getConfigBoolean("CaptchaOptions.Events.BlockBreakEvent.UseData")) {
				isLegacy = true;
				for (String line : Config.getConfigStringList("CaptchaOptions.Filter")) {
					legacyBlocks.add(line);
				}
				return;
			} else {
				isLegacy = false;
				for (String line : Config.getConfigStringList("CaptchaOptions.Filter")) {
					blocks.add(line);
				}
				return;
			}
		} else {
			return;
		}
	}

	@EventHandler
	private void blockBreakEvent(BlockBreakEvent e) {
		if (e.getPlayer() != null) {
			Player player = e.getPlayer();
			if ((isLegacy && legacyBlocks.size() == 0) || (!isLegacy && blocks.size() == 0)) {
				if (!CooldownHandler.isCooldown(player) && !DelayHandler.isDelay(player) && !GUI.isCaptcha(player)) {
					Random random = new Random();
					int a = random.nextInt(chance);
					int b = random.nextInt(chance);
					if (a == b) {
						DelayHandler.openCaptcha(player, false);
						return;
					} else {
						return;
					}
				} else {
					return;
				}
			} else {
				if (isLegacy) {
					if (legacyBlocks.size() > 0) {
						Block block = e.getBlock();
						Material mat = block.getType();
						@SuppressWarnings("deprecation")
						byte data = block.getData();
						String type = mat.name() + ":" + data;
						if ((!invert && !legacyBlocks.contains(type)) || (invert && legacyBlocks.contains(type))) {
							if (!CooldownHandler.isCooldown(player) && !DelayHandler.isDelay(player) && !GUI.isCaptcha(player)) {
								Random random = new Random();
								int a = random.nextInt(chance);
								int b = random.nextInt(chance);
								if (a == b) {
									DelayHandler.openCaptcha(player, false);
									return;
								} else {
									return;
								}
							} else {
								return;
							}
						} else {
							return;
						}
					} else {
						return;
					}
				} else {
					if (blocks.size() > 0) {
						Block block = e.getBlock();
						Material mat = block.getType();
						String type = mat.name();
						if ((!invert && !blocks.contains(type)) || (invert && blocks.contains(type))) {
							if (!CooldownHandler.isCooldown(player) && !DelayHandler.isDelay(player) && !GUI.isCaptcha(player)) {
								Random random = new Random();
								int a = random.nextInt(chance);
								int b = random.nextInt(chance);
								if (a == b) {
									DelayHandler.openCaptcha(player, false);
									return;
								} else {
									return;
								}
							} else {
								return;
							}
						} else {
							return;
						}
					} else {
						return;
					}
				}
			}
		} else {
			return;
		}
	}
}