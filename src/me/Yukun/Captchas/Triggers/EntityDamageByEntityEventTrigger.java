package me.Yukun.Captchas.Triggers;

import java.util.ArrayList;
import java.util.Random;

import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;

import me.Yukun.Captchas.Config;
import me.Yukun.Captchas.GUI;
import me.Yukun.Captchas.Handlers.CooldownHandler;
import me.Yukun.Captchas.Handlers.DelayHandler;

public class EntityDamageByEntityEventTrigger implements Listener {
	private static int chance = Config.getConfigInt("CaptchaOptions.Events.EntityDamageByEntityEvent.Chance");
	private static boolean spawner = Config.getConfigBoolean("CaptchaOptions.Events.EntityDamageByEntityEvent.Spawner");
	private ArrayList<Entity> spawned = new ArrayList<Entity>();

	public static void reload() {
		chance = Config.getConfigInt("CaptchaOptions.Events.EntityDamageByEntityEvent.Chance");
		spawner = Config.getConfigBoolean("CaptchaOptions.Events.EntityDamageByEntityEvent.Spawner");
		return;
	}

	@EventHandler
	private void entityDamageByEntityEvent(EntityDamageByEntityEvent e) {
		if (e.getDamager() instanceof Player) {
			if (!(e.getEntity() instanceof Player)) {
				Player player = (Player) e.getDamager();
				if (!CooldownHandler.isCooldown(player) && !DelayHandler.isDelay(player) && !GUI.isCaptcha(player)) {
					Random random = new Random();
					int a = random.nextInt(chance);
					int b = random.nextInt(chance);
					if (a == b) {
						if (!spawner) {
							DelayHandler.openCaptcha(player, false);
							return;
						} else {
							if (spawned.contains(e.getEntity()) || e.getEntity().getCustomName() != null && e.getEntity().isCustomNameVisible()) {
								DelayHandler.openCaptcha(player, false);
								return;
							}
						}
					} else {
						return;
					}
				} else {
					return;
				}
			} else {
				return;
			}
		} else {
			return;
		}
	}

	@EventHandler
	private void entitySpawnerSpawnEvent(CreatureSpawnEvent e) {
		if (e.getSpawnReason() == SpawnReason.SPAWNER) {
			spawned.add(e.getEntity());
			return;
		}
	}

	@EventHandler
	private void entityDeathEvent(EntityDeathEvent e) {
		if (spawned.contains(e.getEntity())) {
			spawned.remove(e.getEntity());
			return;
		}
	}
}