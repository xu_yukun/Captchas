package me.Yukun.Captchas;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class Config {
	public static Integer getConfigInt(String path) {
		return Main.settings.getConfig().getInt(path);
	}

	public static Double getConfigDouble(String path) {
		return Main.settings.getConfig().getDouble(path);
	}

	public static Boolean getConfigBoolean(String path) {
		return Main.settings.getConfig().getBoolean(path);
	}

	public static String getConfigString(String path) {
		return Main.settings.getConfig().getString(path);
	}

	public static ArrayList<String> getConfigConfigurationSection(String path) {
		if (Main.settings.getConfig().isConfigurationSection(path)) {
			ArrayList<String> list = new ArrayList<String>();
			list.addAll(Main.settings.getConfig().getConfigurationSection(path).getKeys(false));
			return list;
		} else {
			return null;
		}
	}

	public static List<String> getConfigStringList(String path) {
		try {
			return Main.settings.getConfig().getStringList(path);
		} catch (NullPointerException e) {
			return null;
		}
	}

	public static String getMessageString(String path) {
		return Main.settings.getMessages().getString(path);
	}

	public static void setConfigString(String path, String msg) {
		Main.settings.getConfig().set(path, (Object) msg);
		Main.settings.saveConfig();
	}

	public static String getItemsString(String path) {
		return Main.settings.getItems().getString(path);
	}

	public static ArrayList<String> getItemsConfigurationSection(String path) {
		ArrayList<String> section = new ArrayList<String>();
		for (String line : Main.settings.getItems().getConfigurationSection(path).getKeys(false)) {
			section.add(line);
			continue;
		}
		return section;
	}

	public static ArrayList<Player> getPlayers() {
		ArrayList<Player> players = new ArrayList<Player>();
		if (Main.settings.getPlayers().getStringList("Players") != null) {
			for (String names : Main.settings.getPlayers().getStringList("Players")) {
				Player player = Bukkit.getPlayer(UUID.fromString(names));
				players.add(player);
				continue;
			}
			return players;
		} else {
			return null;
		}
	}

	public static void addPlayer(Player player) {
		if (getPlayers() != null) {
			ArrayList<String> set = (ArrayList<String>) Main.settings.getPlayers().getStringList("Players");
			set.add(player.getUniqueId().toString());
			Main.settings.getPlayers().set("Players", set);
			Main.settings.savePlayers();
			return;
		} else {
			ArrayList<String> set = new ArrayList<String>();
			set.add(player.getUniqueId().toString());
			Main.settings.getPlayers().set("Players", set);
			Main.settings.savePlayers();
			return;
		}
	}
}