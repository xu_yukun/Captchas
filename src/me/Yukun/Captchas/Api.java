package me.Yukun.Captchas;

import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

public class Api {
	public static Plugin plugin = Bukkit.getServer().getPluginManager().getPlugin("Captchas");

	public static String replaceStrikes(Player player, String msg) {
		if (GUI.getWrong(player) != null) {
			return msg.replace("%wrong%", GUI.getWrong(player) + "");
		} else {
			return msg.replace("%wrong%", 0 + "");
		}
	}

	public static String replaceItemName(String msg, String mat) {
		String matdname = Config.getItemsString("Items." + mat);
		return msg.replace("%item%", matdname);
	}

	public static int getRandomNumber(int range) {
		Random random = new Random();
		return random.nextInt(range);
	}

	@SuppressWarnings("deprecation")
	public static ItemStack getItemInHand(Player player) {
		if (getVersion() >= 191) {
			return player.getInventory().getItemInMainHand();
		} else {
			return player.getItemInHand();
		}
	}

	public static String color(String msg) {
		return ChatColor.translateAlternateColorCodes('&', msg);
	}

	public static String removeColor(String msg) {
		msg = ChatColor.stripColor(msg);
		return msg;
	}

	public static Integer getVersion() {
		String ver = Bukkit.getServer().getClass().getPackage().getName();
		ver = ver.substring(ver.lastIndexOf('.') + 1);
		ver = ver.replaceAll("_", "").replaceAll("R", "").replaceAll("v", "");
		return Integer.parseInt(ver);
	}

	public static boolean isInt(String s) {
		try {
			Integer.parseInt(s);
		} catch (NumberFormatException nfe) {
			return false;
		}
		return true;
	}
}